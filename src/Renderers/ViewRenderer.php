<?php

/*
 * This file is part of the yii2-render-negotiator.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-render-negotiator
 */

namespace RobotE13\Yii2RenderNegotiator\Renderers;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface ViewRenderer
{
    /**
     *
     * @param string $view the view name. Оставлен для совместимости с вызовом этого метода в контексте контроллера.
     * @param array $params the parameters (name-value pairs) that will be extracted and made available in the response.
     * @param object $context the context to be assigned to the view and can later be accessed via [[context]] in the view.
     * @return string the rendering result
     */
    public function render($view, $params = [], $context = null);
}
