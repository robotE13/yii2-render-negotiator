<?php

/**
 * This file is part of the yii2-render-negotiator.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-render-negotiator
 */

namespace RobotE13\Yii2RenderNegotiator\Renderers;

//use Yii;

/**
 * Description of JsonView
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class ParamsRenderer extends \yii\base\Component implements ViewRenderer
{

    public function render($view, $params = [], $context = null)
    {
        $context->layout = false;
//        $response = Yii::$app->getResponse();
//        $response->format = Response::FORMAT_JSON;
        return $params;
    }

}
