<?php

/**
 * This file is part of the yii2-render-negotiator.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-render-negotiator
 */

namespace RobotE13\Yii2RenderNegotiator;

use Yii;

/**
 * Description of ContentNegotiator
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class RenderNegotiator extends \yii\filters\ContentNegotiator
{

    /**
     * @var string|array the configuration for creating the serializer that formats the response data.
     */
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    public function beforeAction($action)
    {
        parent::beforeAction($action);
        if(Yii::$app->response->format !== \yii\web\Response::FORMAT_HTML)
        {
            $action->controller->setView(new Renderers\ParamsRenderer());
        }
        return true;
    }

    public function afterAction($action, $result)
    {
        if(!$action->controller->view instanceof Renderers\ViewRenderer)
        {
            return $result;
        }

        $serializer = Yii::createObject($this->serializer);
       // throw new \Exception(\yii\helpers\VarDumper::dumpAsString($result));
        return $serializer->serialize($result);
    }

}
